# re2c conda recipe

Home: http://re2c.org

Package license: Public domain

Recipe license: BSD 3-Clause

Summary: re2c is a free and open-source lexer generator for C and C++
